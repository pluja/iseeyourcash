# [iseeyour.cash](https://iseeyour.cash)

See social network users cryptocurrency addresses and balances. Powered by transparent blockchains. https://iseeyour.cash/

### ✅ Implemented
- [x] Support for Twitter
- [x] Use a database to improve speed.
- [x] Bitcoin and Ethereum famous addresses blacklist.
- [x] Check only new comments instead of all comments every time.

### ⚙️ Coming
- [ ] Support for Reddit users
- [ ] Addresses by user list
- [ ] Reverse address search (search addresses, find users)

### ✨ Self-hosting

To self-host your [iseeyour.cash](https://iseeyour.cash) instance, please refer to [the wiki](https://codeberg.org/pluja/iseeyourcash/wiki/Set-up-a-self-hosted-instance)
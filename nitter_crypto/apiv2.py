from pymongo import MongoClient
import pymongo

from dotenv import load_dotenv
from nitter_crypto.pnytter.pnytter import Pnytter

from datetime import datetime, timedelta
import pprint
import json
import re
import os

load_dotenv()
blacklistFile = open("data/blacklist.json")
blacklist = json.load(blacklistFile)['blacklist']

nitter_instances=os.getenv("NITTER_INSTANCES").strip('][').strip('"').split(', ')
pnytter = Pnytter(
  nitter_instances=nitter_instances
)

print(nitter_instances)

addr_regex = {
    "btc": re.compile(r"\b[bc1|13][a-zA-HJ-NP-Z0-9]{25,42}\b"),
    "eth": re.compile(r"\b(0x[a-fA-F0-9]{40})\b"),
    "xno": re.compile(r"\b(nano_[13][1-9a-z]{59})\b")
}

connString = os.getenv("MONGO_STRING")
client = MongoClient(connString)
db = client.iseeyourcash
usersDB = db.users

def getProfileAddresses(username):
    # Remove conflicting characters from username
    username = re.sub(r"(?:(?:/)?\bu)?/|@|\s", "", username)

    # Check if username is invalid
    invalid = re.compile(r"[^A-Za-z0-9_\-\.]")

    print(username)
    # If the user is invalid, return
    if invalid.findall(username):
        print("Invalid username")
        return False

    # Get max values
    scan_from_inception = os.getenv("SCAN_FROM_INCEPTION")
    if scan_from_inception == "False":
        scan_from_inception = False
    else:
        scan_from_inception = True

    scan_max_days = int(os.getenv("SCAN_DAYS_NUMBER"))
    max_default_tweets = int(os.getenv("MAX_DEFAULT_TWEETS"))
    
    # If the database is not empty
    if usersDB.count_documents({}) > 0:
        # If the user exists in the database
        userObj = usersDB.find_one({'username':username})
        if userObj:
            # We get its last tweet id
            from_date = userObj['last_created_on']
            last_tweet_id = userObj['last_tweet_id']

        # Otherwise we set a far-away date to get all tweets until today.
        else:
            # Check if user exists
            try:
                user = pnytter.find_user(username)
                if not user:
                    print("User does not exist")
                    return False
            except Exception as e:
                print(e)
                return False

            # If the user has less than 200 tweets, we will scan all of them.
            if user.dict()['stats']['tweets'] < max_default_tweets:
                scan_from_inception = True

            if scan_from_inception:
                from_date = datetime.strptime('Jan 09 2009  1:30PM', '%b %d %Y %I:%M%p').strftime("%Y-%m-%d")
            else:
                from_date = (datetime.utcnow() - timedelta(days=scan_max_days+1)).strftime("%Y-%m-%d")
            last_tweet_id = "0"
    else: # Otherwise, we need to initialize the DB
        if scan_from_inception:
            from_date = datetime.strptime('Jan 09 2009  1:30PM', '%b %d %Y %I:%M%p').strftime("%Y-%m-%d")
        else:
            from_date = (datetime.utcnow() - timedelta(days=(scan_max_days+1))).strftime("%Y-%m-%d")
        last_tweet_id = "0"
    
    to_date = (datetime.utcnow() + timedelta(days=1)).strftime("%Y-%m-%d")
    tweets = pnytter.get_user_tweets_list(username, filter_from=from_date, filter_to=str(to_date))
    # Final result object
    addresses = {
        "btc": [],
        "eth": [],
        "xno": []
    }

    if len(tweets) > 0:
        for tweet in tweets:
            if last_tweet_id != tweet.tweet_id:
                for key in addr_regex.keys():
                    found_addresses = addr_regex[key].findall(str(tweet.text))
                    if len(found_addresses) > 0:
                        for address in found_addresses:
                            if not address in blacklist:
                                addresses[key].append((address, str(tweet.tweet_id)))
        return addresses, tweets[0].created_on.strftime("%Y-%m-%d"), tweets[0].tweet_id
    else:
        return addresses, from_date, last_tweet_id
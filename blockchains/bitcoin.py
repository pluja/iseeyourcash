import json
import requests

def getUpdateBtcPriceUSD():
    url = "https://api.coindesk.com/v1/bpi/currentprice/USD.json"
    req = requests.get(url)
    price = json.loads(req.text)
    usd_rate = price['bpi']['USD']['rate_float']
    return usd_rate

def fromBS(address):
    url = f"https://blockstream.info/api/address/{address}"
    try:
        req = requests.get(url)
    except Exception as e:
        print(e)
        url = f"https://btcscan.org/api/address/{address}"
        try:
            req = requests.get(url)
        except Exception as e:
            print(e)
            return -1

    address = json.loads(req.text)
    address_balance = address['chain_stats']['funded_txo_sum']
    return address_balance

def getAddressBalance(address):
    balance = fromBS(address)
    if balance == -1:
        return -1

    balance = float(balance*0.00000001)
    rate = getUpdateBtcPriceUSD()
    balance = balance*rate
    return (balance // 0.01 / 100)
FROM python:3.9-bullseye AS base
# Image to Build Dependencies
FROM base AS builder

WORKDIR /app

COPY ./requirements.txt /app
RUN pip install --prefix=/install -r requirements.txt

# Runtime Environment Image
FROM base

ENV ISYC_DEBUG=0

WORKDIR /app
COPY --from=builder /install /usr/local
COPY . /app/

EXPOSE 1338
CMD sanic iseeyourcash.app --host=0.0.0.0 --port=1338 --workers=4
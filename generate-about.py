import markdown

with open('static/about.md', 'r') as f:
    text = f.read()
    html = markdown.markdown(text)

with open('templates/about.html', 'w') as f:
    head = """
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../static/fonts/fa/css/all.min.css">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <link href="../static/css/cryptofont.css"rel="stylesheet">
        <title>About ISYC</title>
    </head>
    <body>
    """
    style = """
    <style>
        @font-face {
            font-family: 'blunext';
            src: url('../static/fonts/Minipax-Regular.ttf');
        }

        html, body{
            margin: 0;
            padding: 1em;
            font-family: sans-serif;
        }

        html{
            background-color: rgb(38, 39, 46);
            color: rgb(255, 255, 255);
        }

        h2{
            font-family: blunext;
        }

        p{
            margin: 8px !important;
        }

        a{
            color: rgb(232, 185, 14) !important;
        }

        .mono{
            font-family: monospace;
        }

        code{
            height: auto;
            font-weight: bold;
            max-height: 200px;
            overflow: auto;
            padding: 1px;
            display: inline;
            word-break: normal !important;
            word-wrap: normal !important;
            white-space: pre !important;
        }​

        .container{
            padding: 2em;
        }

        .small_button{
            margin: 1em;
            margin-bottom: 0 !important;
            font-size: small;
            padding: .3em;
            max-width: 30px;
            text-align: center;
            cursor: pointer;
        }

        a .small_button{
            color: white;
            text-decoration: none;
        }
    </style>
    """
    bottom = """
    </body>
    </html>
    """

    html = head + html + style + bottom
    f.write(html)
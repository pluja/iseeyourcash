from nitter_crypto import apiv2 as nitter
from pymongo import MongoClient
from dotenv import load_dotenv
from datetime import datetime
from blockchains import bitcoin
import asyncio
import os

load_dotenv()
connString = os.getenv("MONGO_STRING")
client = MongoClient(connString)

def updateCreateUser(username):
    #client = motor.motor_tornado.MotorClient(connection, serverSelectionTimeoutMS=5000)
    db = client.iseeyourcash
    usersDB = db.users
    addressesDB = db.addresses
    # If the user <username> does not exist in the database,
    # it is created with the found addresses.
    # Otherwise, the user and the addresses are updated
    
    # Get the addresses for the username in async function
    addresses, last_created_on, last_tweet_id = nitter.getProfileAddresses(username)
    print(addresses)
    if addresses == -1:
        return -1

    # If the user is not in the database
    if not usersDB.find_one({"username": username}):
        print("User is not in the database")
        # New user initialization
        newUser = {
                    "username": "username",
                    "addresses": {
                        "btc": [],
                        "eth": [],
                        "xno": []
                    },
                    "last_created_on": "",
                    "last_tweet_id": "",
                    "last_check": "2022-05-10T16:13:41.883637"
                }
        newUser['username'] = username
        newUser['last_created_on'] = last_created_on
        newUser['last_tweet_id'] = last_tweet_id
        newUser['last_check'] = datetime.utcnow().strftime('%Y-%m-%dT%H:%M')

        print(newUser)
        
        # For each currency
        for currency in addresses.keys():
            # If there are found addresses
            if len(addresses[currency]) > 0:
                # For each currency address
                for a in addresses[currency]:
                    # If the addres has not already been added
                    if not a[0].lower() in newUser['addresses'][currency]:
                        # We add it to the user currency addresses list
                        # If the address is not in the addresses DB, we create it
                        if not addressesDB.find_one({'address': a[0].lower()}):
                            newAddress = {
                                            "address": "",
                                            "users": [],
                                            "balance": 0,
                                            "currency": "",
                                            "urls": []
                                        }
                            newAddress['address'] = a[0].lower()
                            newAddress['users'].append(username)
                            newAddress['currency'] = currency
                            newAddress['urls'].append(f"/{username}/status/{a[1]}")
                            if newAddress['currency'] == "btc":
                                newAddress['balance'] = bitcoin.getAddressBalance(a[0].lower())
                            addressesDB.insert_one(newAddress)
                        # Else, we update the URLs with the new URLs we found.
                        else:
                            updateAddress = addressesDB.find_one({'address': a[0].lower()})
                            if not addressesDB.find_one({'address': a[0].lower(), 'url': f"/{username}/status/{a[1]}"}):
                                updateAddress['urls'].append(f"/{username}/status/{a[1]}")
                            if not addressesDB.find_one({'users': username, 'address': a[0].lower()}):
                                updateAddress['users'].append(username)

                        newUser['addresses'][currency].append(a[0].lower())
                    else:
                        if addressesDB.find_one({'address': a[0].lower()}):
                            if not addressesDB.find_one({'urls': a[1]}):
                                updateAddress = addressesDB.find_one({'address': a[0].lower()})
                                updateAddress['urls'].append(a[1])
                                addressesDB.update_one({"address":a[0].lower()}, {"$set": {"urls": updateAddress['urls']}})

        # Insert to the database
        usersDB.insert_one(newUser)
    
    # If the user is found in the database
    else:
        # We retrieve the user from the database
        updateUser = usersDB.find_one({"username": username})
        # Update the last_check value
        updateUser['last_check'] = datetime.utcnow().strftime('%Y-%m-%dT%H:%M')
        # For each currency
        for currency in addresses.keys():
            # If there are found addresses
            if len(addresses[currency]) > 0:
                # For each currency address
                for a in addresses[currency]:
                    # If the address has is already present we skip
                    if a[0].lower() in str(updateUser['addresses'][currency]):
                        if addressesDB.find_one({'address': a[0].lower()}):
                            if not addressesDB.find_one({'urls': a[1]}):
                                updateAddress = addressesDB.find_one({'address': a[0].lower()})
                                updateAddress['urls'].append(a[1])
                                addressesDB.update_one({"address":a[0].lower()}, {"$set": {"urls": updateAddress['urls']}})
                        continue

                    # Else we update the address list
                    # If the address is already in the database, we update
                    if addressesDB.find_one({'address': a[0].lower()}):
                        updateAddress = {
                                            "address": "",
                                            "users": [],
                                            "balance": 0,
                                            "currency": "",
                                            "urls": []
                                        }
                        updateAddress['users'].append(username)
                        updateAddress['urls'].append(a[1])
                        addressesDB.update_one({"address":a[0].lower()}, {"$set": {"urls": updateAddress['urls']}})
                        addressesDB.update_one({"address":a[0].lower()}, {"$set": {"users": updateAddress['users']}})
                        updateUser['addresses'][currency].append(a[0].lower())

        # Update the user data
        if last_tweet_id:
            result = usersDB.update_one({"username":username}, {"$set": {"last_created_on": last_created_on}})
            result = usersDB.update_one({"username":username}, {"$set": {"last_tweet_id": last_tweet_id}})
        result = usersDB.update_one({"username":username}, {"$set": {"addresses": updateUser['addresses']}})
        result = usersDB.update_one({"username":username}, {"$set": {"last_check": updateUser['last_check']}})




# Defining main function
#'''
#def main():
#    updateCreateUser("kycnot")
#  
#  
# Using the special variable 
# __name__
#if __name__=="__main__":
#    main()
#'''
# Server
from sanic.response import html, text
import sanic
from sanic import Sanic

from pymongo import MongoClient
import os

# Utils
import re
import random
from bs4 import BeautifulSoup
import asyncio

from jinja2 import Environment, FileSystemLoader
from datetime import datetime, timedelta
from urllib.parse import unquote
import time

from random import randrange
import os, os.path
import mongo_database as mdb

from dotenv import load_dotenv

load_dotenv()
connString = os.getenv("MONGO_STRING")

client = MongoClient(connString)
db = client.iseeyourcash
usersDB = db.users
addressesDB = db.addresses

base_dir = os.path.abspath(os.path.dirname(__name__))
static_dir = os.path.join(base_dir, 'static')
templates_dir = os.path.join(base_dir, 'templates')
data_dir = os.path.join(base_dir, 'data')
env = Environment(loader=FileSystemLoader(templates_dir), autoescape=True)
app = Sanic(__name__)

app.static('/static', static_dir)
#app.config.DEBUG = int(os.environ["ISYC_DEBUG"]) > 0

#f = open(f'{data_dir}/data.json')
#data = json.load(f)['quotes']


@app.route('/favicon.ico', name="favicon")
def favicon(request):
    return sanic.response.file(f"{static_dir}/icons/favicon.png", mime_type='image/vnd.microsoft.icon')

#filename = ""
@app.route("/", name="root")
@app.route("/index", name="index")
async def index(request):
    template = env.get_template('index.html')
    quote = "Processing..."
    if(request.args):
        args = request.args
        err=args.get("error")
    
    total_addresses = addressesDB.count_documents({})
    return html(template.render(quote=quote, total_addresses = total_addresses))

@app.route("/about", name="about")
async def about(request):
    template = env.get_template('about.html')
    return html(template.render())


@app.get("/q")
@app.get("/<query>", name="getaddr")
async def user(request, query=None):
    if(request.args):
        args = request.args
        query=args.get("q")
    
    # Remove invalid characters from query
    query = re.sub(r"[^A-Za-z0-9_\-]", "", query)
    
    # Check if search query is an address
    addr_patterns = {
        "btc": re.compile(r"\b[bc1|13][a-zA-HJ-NP-Z0-9]{25,39}\b"),
        "eth": re.compile(r"\b(0x[a-fA-F0-9]{40})\b"),
        "xno": re.compile(r"\b(nano_[13][1-9a-z]{59})\b")
    }
    template = env.get_template('address_results.html')
    for key in addr_patterns.keys():
        if addr_patterns[key].findall(str(query.lower())):
            address = addressesDB.find_one({'address': query.lower()})
            return html(template.render(user=False, address=address))

    template = env.get_template('user_results.html')
    user = usersDB.find_one({'username':query})
    if user: # User exists in DB
        # We get all the addresses associated to the user, and convert the cursor to a list.
        addresses = list(addressesDB.find({"users":user['username']}))

        # Only if the check was made more than an hour ago, it will be checked again.
        if datetime.strptime(user['last_check'], '%Y-%m-%dT%H:%M')+timedelta(minutes=int(os.getenv('RESCAN_MINUTES'))) < datetime.utcnow():
            # We update the user.
            result = mdb.updateCreateUser(user['username'])

            # If the result is -1, the user does not exist.
            if result == -1:
                return html(template.render(user=query, non_existent=True))
            
            # We retrieve the user from the database.
            user = usersDB.find_one({'username':user['username']})
            return html(template.render(user=user, addresses=addresses))

        else: # If the user has been recently checked
            user = usersDB.find_one({'username':user['username']})
            return html(template.render(user=user, addresses=addresses))

    else: # User does not exist in DB
        # We update the user.
        result = mdb.updateCreateUser(query)

        if result == -1:
            return html(template.render(user=query, non_existent=True))

        user = usersDB.find_one({'username':query})
        addresses = list(addressesDB.find({"users":user['username']}))
        return html(template.render(user=user, addresses=addresses))

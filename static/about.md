[[Go back]](https://iseeyour.cash)

# Iseeyour.cash FAQ

## What is this?

Iseeyour.cash is a search engine. You can search for social networks¹ users, and it will automatically scan their profile and try to find any cryptocurrency address that have been published (if any) for that user (currently BTC, ETH and XNO addresses are supported). All results are stored in a database, so even if the user deletes the content, if it was once scrapped, it will still be stored in the database.

For each result, a link to the content where it was found is provided, along with a link to a link to the blockchain scanner to see the address transaction history, funds and interactions. Whenever is possible Iseeyour.cash will also try to provide information about the total address balance.

You can try with my Twitter username: `kycnot`

> Note: The kycnot addresses are not owned by me. Do not send funds! In case you want to donate, see the [Support section](#support) below.

---

## How does it work?

Just enter a username or an address in the search box and wait!

If the user has less than 250 tweets, Iseeyour.cash will scrap the whole user history. If the user has more than 300 tweets, it will scan the last 10 days of tweets from that user. This **limitation** is set to avoid overloading the server and the Nitter instance.

Currently, it only supports **Twitter** users. But **Reddit** integration should come soon. [See the roadmap](#roadmap)

### Reverse search

Reverse search is also possible. You can search for an address, and it will show a list of users that have shared it, if any.

### Does it overload a public Nitter instances?

No, I am not using any of the public instances. A dedicated local Nitter instance is running on the Iseeyour.cash server, and it handles all the traffic.

---

## How is this useful?

Well, it is more a way to make cryptocurrency users conscious of the privacy [risks of **transparent blockchains**](#transparent-blockchains) than the utility this can bring.

---

## Why this happens?

Most cryptocurrencies' blockchains are **transparent**. This means that every bit of data in the blockchain is available for everyone to see. This makes sense since the **nodes** and the **miners** of the network must be able to review everything in order to make the blockchain work (i.e. making sure a transaction is valid).

You might say that addresses are anonymous since they are not tied to any **personal** identity. But this is not true, such addresses are not **anonymous** but **pseudonymous**. If anybody gets to link an address to your personal identity somehow, this address will be tied to your identity. So addresses in **transparent blockchains** always act as *pseudonyms* to your identity.

For example, in the event you publish your address to any social network. Or if you pass through a [KYC process](https://kycnot.me/about#kyc) on an exchange and then transact from and/or to your address. Or maybe a friend with a KYC'ed address sends you an amount to your address and leaks some information somewhere... The posibilities for linking your identity are endless.

[Monero](https://getmonero.org) is a cryptocurrency that, with the help of cryptography and math, achieves an opaque blockchain just as secure as a **transparent** one, but cutting all the threats tied to **pseudonimity**. It is worth noting that even [Satoshi Nakamoto](https://wikipedia.org/wiki/Satoshi_Nakamoto) already talked about [privacy features](https://farside.link/teddit/r/Monero/comments/u8e5yr/satoshi_nakamoto_talked_about_privacy_features/) that got implemented in Monero.

You can [read this link](https://github.com/monerobook/monerobook/blob/second-edition/chapters/1.md#132-real-life-use-cases-for-monero) to find a list of possible real-work use cases for Monero and how transparent blockchains can harm a user.

---

## Why such limits?

On the first place, the server I have for Iseeyour.cash is not very powerful. I may adjust the limits seeing what it can handle, but it may not be much more than what it is now. I am planning to acquire a much more powerful server in a near future, but I have to wait until I can afford it.

### I want to scrap the full history of a user without limits!

This **is possible** if you **self-host** your own Iseeyour.cash instance.

When self-hosting, you can edit the `.env` file and enable the `SCAN_FROM_INCEPTION` option. This will scan all the **tweets** from a given user since Bitcoin started on January 2009. You can also change the `SCAN_DAYS_NUMBER` limit to increase the number of days to scan a user, or the `MAX_DEFAULT_TWEETS` to increase the `300` tweet default limit.

---

## Roadmap

- [ ] Add support for Reddit users scraping.
- [ ] Show full balance for all supported currencies.

---

## About me

I host and maintain several privacy-related open-source projects:

- [kycnot.me](https://kycnot.me) - Find the best anonymous and KYC-free exchanges and services accepting Bitcoin and Monero.
- [Awesome Privacy](https://github.com/pluja/awesome-privacy) - Curated list of privacy-respecting alternatives to privacy-harmful software.
- [iseeyour.cash](https://iseeyour.cash) - Search and see Twitter users' cryptocurrency addresses and balance.
- [r3d.red](https://r3d.red) - Public instances for some services like Nitter, Teddit, Microbin and Feetter.
- [Feetter](https://ft.r3d.red) - Manage your Nitter feeds.

### Support

If you like what I do and want to support is much my work, it will be much appreciated as it helps me cover the **server** and **domain** costs. 

You can make your donations to the addresses you can find in [KYCNOT.me support page](https://kycnot.me/about#support).